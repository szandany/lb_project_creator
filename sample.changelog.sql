--liquibase formatted sql

--Formatted SQL files use comments to provide Liquibase with metadata.
--All formatted SQL changelogs must begin with the comment "liquibase formatted sql" on the first line.
--Each changeset begins with a comment of the form "changeset username:changesetID"
--The username and changesetID combination must be unique within the file.
--The changeset comment is followed by one or more SQL statements.
--Optional:  You may specify code to rollback the changeset using the "rollback" comment

--changeset your_name:changeset_id_1 labels:release1.0 context:all
create table liquibase_person (
    id int primary key,
    name varchar(50) not null,
    address1 varchar(50),
    address2 varchar(50),
    city varchar(30)
)
--rollback drop table liquibase_person

--changeset your_name:changeset_id_2 labels:release1.0 context:all
create table liquibase_company (
    id int primary key,
    name varchar(50) not null,
    address1 varchar(50),
    address2 varchar(50),
    city varchar(30)
)
--rollback drop table liquibase_company

